# Deployment
## 1. Overview
Almost every project will deploy one or more containers to the Kubernetes cluster.  
This is done by implementing an appropriate pipeline (_.gitlab-ci.yml_) within the project's repository that chooses the target-stage for deployment by taking the branch name into account (see [Gitlab Branching Strategy and Naming Convention](gitlab.md) for more information on branch names).  

A pipeline will have a stage named _**deploy**_ and will have up to 4 Jobs within this stage, some of them are started manually while others will run automatically, depending on the branch name:
  
**Example ".gitlab-ci.yml":**
```yaml
... 

stages:
  ... 
  - deploy

... 

do deploy:
  tags:
    - kubernetes
  image: bitnami/kubectl:latest
  stage: deploy
  script:
    # here bash commands are executed for deploying the image
    - kubectl apply ...

...
```  
## 2. Choosing the correct Cluster and Namespace
The actual deployment is done by calling the `kubectl`'s `apply`-command with a given deployment.yaml within the job's `script`-section.\
The `kubectl`-command expects a `kubecfg`-parameter in order to get access to the cluster and also a namespace (`-n`) parameter.\
In order to get the deployment as flexible, as possible, both parameters are defined as Gitlab-Variables. Within the pipeline, those variables are used instead of hardcoding the kubecfg and namespace:
```  
do deploy:
  tags:
    - kubernetes
  image: bitnami/kubectl:latest
  stage: deploy
  script:
    # since kubectl expects the kubecfg as a file, read the 
    # Gitlab-Variable and write to a temporary file:
    - echo "$KUBE" > "$(pwd)/kube.config"
    - kubectl apply -f deployment.yaml -n ${NAMESPACE} --kubeconfig "$(pwd)/kube.config"

```  
## 3. Deploying to the Stages
### 3.1 Deploying to the Dev Stage
The dev stage is the "default" stage and will be the target stage from within the master branch of a project.\
Components in the master-branch are "under development" and should be deployed only to the dev-stage.\
For this to end, the deploy job has to filter the branch-name for "master":
```yaml
do deploy dev-stage:
  ...
  only:
    - master
  ...
```  
 
### 3.2 Deploying to the Staging Stage
The staging stage is where software components are deployed to and where QA and/or integrations to other components can take place.\
The components are in the status "Release Candidate" and the branch-name starts with a capital 'C' (see [Gitlab Branching Strategy and Naming Convention](gitlab.md) for more information).\
In order to deploy to the staging-stage, the deploy job has to filter the branch-name for a capital 'C' in the beginning:
```yaml
do deploy staging-stage:
  ...
  only:
    - /^C./

  ...
```  

### 3.3 Deploying to the Production Stage
The production stage is the final stage, where components from the staging-stage are deployed to.\
The components are in the status "Released" and the branch-name starts with a capital 'R' (see [Gitlab Branching Strategy and Naming Convention](gitlab.md) for more information).\
In order to deploy to the production-stage, the deploy job has to filter the branch-name for a capital 'R' in the beginning:
```yaml
do deploy production-stage:
  ...
  only:
    - /^R./

  ...
```  
> **_NOTE:_** For safety reasons, the "deploy to production"-job should be triggered manually. This can be done by adding the manual-only tag to the job:
>  ```yaml
>  do deploy production-stage:
>    ...
>    only:
>      - /^R./
>    when: manual
> 
>    ...
>  ```  
