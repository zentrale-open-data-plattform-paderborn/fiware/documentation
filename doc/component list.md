# Component List

## Tenant Manager
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/tenantmanager/-/blob/master/README.md
***
## Grafana
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/grafana/-/blob/master/README.md
***
## Orion Context broker
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/orion/-/blob/master/README.md
***
## QuantumLeap
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/quantumleap/-/blob/master/README.md
***
## User Management - Keyrock
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/keyrock/-/blob/master/README.md
***
## API Management
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/platform/-/blob/master/README.md
***
## API Umbrella - proxy
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/umbrella/-/blob/master/README.md
***
## Perseo
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/perseo/-/blob/master/README.md
***
## Knowage
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/knowage/-/blob/master/README.md
***
## Kurento
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/kurento/-/blob/master/README.md
***
## Cosmos
Gitlab: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/cosmos/-/blob/master/README.md
