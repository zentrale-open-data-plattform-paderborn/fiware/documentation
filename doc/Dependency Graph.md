# Dependency Graph

Following diagram illustrates how components access the installed databases and persistent volumes.
* Blue arrows denote sensor- and other data, stored in databases.
* Red arrows denote settings- and application data, stored in databases.
* Black arrows denote settings- and application data, stored in persistent volumes.


![](../images/dbaccessview.PNG)

