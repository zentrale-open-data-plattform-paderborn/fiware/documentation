The following sequence diagram shows how data is posted successfully to the platform.

![](datapost200.png)

The following sequence diagram shows how data is posting fails due to user not being authorized to the platform.

![](datapost403.png)