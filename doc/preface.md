# Preface

This GitLab project contains the documentation of the FIWARE/API-Management software stack as setup in the project for the city of Paderborn. It is complemented by the installation and setup instruction (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/platform-setup/README.md) and the official documentation for the FIWARE stack provided by the FIWARE foundation.\

In the first chapter (General) the general deployment of the FIWARE components using Continues Integration (CI) and Continues Deployment (CD) via GitLab towards a Kubernetes cluster is described.\

The second chapter describes the overall architecture of the system including the authentication/authorisation and UML sequence diagrams for the most important flows.   

The third chapter includes the administration documentation. The admin documentation includes descriptions of important admin tasks like user account approvals or tenant promotion as well as links towards the component configuration pages.   

The forth chapter includes a documentation for end users of the FIWARE platform. After a short overview of FIWARE and platform features, the NGSI concept and the tenant system are introduced.  

Chapter five includes detailed "how to"s for regularly tasks like user onboarding.  

A glossary with the domain specific terms completes the documentation.
