# Project Structure
## 1. Overview
Every part (documentation, software component) of the **Open Data Platform** will have it's own Gitlab project, even if - for example - no Software is deployed!

Such a Gitlab project will contain at least a description of the component in the project's README.md-file.

## 2. Documentation Projects
Documentation projects contain documentation pages, written in Markdown (see [Markdown Guide](https://www.markdownguide.org/getting-started/) for more information) only and do not deploy components to the cluster.


## 3. Software Component(s) Projects
At a glance, each software component should reside in its own Gitlab project.\
Each Gitlab project (i.e. it's component or components) will then be deployed on it's own Pod on the Kubernetes cluster.

As long as there are no needs to put more than one container into one pod (e.g. two containers need to share a common host-directory), putting each service into it's own pod will enable us to
* handle each component as an independent part that can be:
  * tested on it's own
  * updated with a newer version with less side-effects
  * exchanged by another software if necessary in the future (as long as the interfaces are compatible)
* scale each component individual 

Each pod has to be deployable independent from other pods. That is: Each Gitlab-Project can be run (here: the project's pipeline) separately and deploy the image(s) to the cluster, without any restriction.  
Naturally there will be dependencies between the services. E.g.: A keyrock Service will depend on a MySQL-Database service that runs in another pod, that may be deployed AFTER the Keyrock service.\
For the initial deployment, these dependencies are handled by the sequence, the components are installed (see [setup documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/platform-setup/README.md#2-deploy-the-components) for more information).
