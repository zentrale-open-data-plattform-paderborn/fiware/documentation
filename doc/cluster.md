# Cluster Requirements
## 1. Stages
The cluster-infrastructure is assumed to provide three stages:
* Development
* Staging
* Production

In order to access the cluster, for each stage:
* **A kubecfg-file is needed**\
  This file is provided by the hoster of the cluster
  > **_NOTE:_** One kubecfg-file may be shared with more than one namespace - depending on the cluster configuration.
* **The name of the namespace is needed**\
  The name is provided by the hoster of the cluster
  > **_NOTE:_** The namespace may be omitted in case, every stage resides within it's own cluster.  
  In this documentation it's assumed to have 3 namespaces called: *dev*, *staging* and *prod*.

## 2. Masquerading
It is assumed that ingress is setup to do masqerading on the incoming IP-addresses.

## 3. Virtual IP
A virtual IP has to be configured for the wanted domain name for the installation. This IP will be used in the Umbrella configuration.

## 4. Ports
It is assumed that only port 443 is opened.

## 5. Cerificates
Certificates are used only by Umbrella. For deployment it's assumed to have 2 k8s-secrets called *umb-privkey* and *umb-fullchain*.

## 6. Persitent volumes
It is assumed the cluster provides a storage class named *storage-class*. 

## 7. Accessing the Cluster
### 7.1 SSH
SSH is not needed.

### 7.2 Kubectl
In order to access the cluster, a tool is needed called **kubectl**.\
For more information on how to install, see [Install and Set Up kubectl](https://kubernetes.io/de/docs/tasks/tools/install-kubectl/) (Windows, Mac & Linux).

After installation, the **kubectl**-command can be used to access the cluster by utilizing the kubecfg-file and taking the namespace into account, if applicable.\
Example:
```bash
$ kubectl cluster-info -n NAMESPACE --kubeconfig "./kube.config"
```
## 8. Databases
A MySQL-Database is assumed to be installed. This MYSQL needs to be accessible through service name 'NAMESPACE-mysql-master', where namespace matches fiware-dev, fiware-staging or fiware-prod, depending on your desired namespace. User credentials for a privileged user are expected. The credentials are provided by a k8s-Secret called *db-secret* looking like:  
```
DATABASE: ***
PASSWORD: ***
ROOT_PASSWORD: ***
USER: ***
```

## 9. Gitlab
A Gitlab instance is needed in order to do the deployment via CI/CD.
