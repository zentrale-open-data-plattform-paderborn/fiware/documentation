# Gitlab Branching Strategy and Naming Convention

## 1. Protected Master Branch
The master-branch of each project is protected. That is, changes in the repository are not pushed directly to the master-branch but are pushed via a _Merge-Request_. The _Merge-Request_ includes a Code-Review, done by the assignee of the _Merge-Request_.

## 2. Naming Convention
Beside the master branch, other branches will be created due to
* **Issues** - if a branch is taken in order to handle an Issue, the name of the branch starts with an 'I' and contains the (global unique) issue number (e.g. `I234`).<br>Issue branches will be merged into the master-branch, once the issue is fixed. The branch will be deleted, when merged. 
* **Features** - if a branch is taken in order to handle a feature update, the name of the branch starts with an 'F' and contains the feature token (e.g. `FAPI-32`).\
Feature tokens can be taken from the name of the work package from the used project management tool (e.g. Jira).<br>Feature branches will be merged into the master-branch when the feature is completed.  The branch will be deleted, when merged.
* **Release Candidates** - Release-Candidate-branches will be created from the master-branch. They are protected, too. A Release-Candidate-Branch starts with a 'C' and contains the version of the release (e.g. `C0.99`)
* **Releases** - Release-branches will be created from the master-branch. They are protected, too. A Release-Branch starts with an 'R' and contains the version of the release (e.g. `R0.99`)

Beside the above branchnames, no other branches are allowed!

> **_NOTE:_** The branchname (here: the first letter) is used for filtering within the CI/CD process. It's crucial to take these naming conventions into account! See [Deployment](deployment.md) for more information.