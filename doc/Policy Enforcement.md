# Policy Enforcement

## 1. General

The API Umbrella acts as a Policy Enforcement Point (PEP) in front of other services within the Platform. It takes the incoming request, inspects it and either accepts or rejects the request. 

In case of Bearer tokens, the tokens are sent to Keyrock for [introspection](https://oauth.net/2/token-introspection/); the API Umbrella on it's own cannot decide if the request should be allowed or denied.

In case of other HTTP headers, API Umbrella has it's internally configured rules, Sub-URL rules. They allow fine grained access control per API. Tenant Manager leverages this internal rule table by adding and modifying the rules when a Tenant is added or modified in the API Management.

X-API-keys (API keys) are bit special; API Umbrella keeps an internal register of users and their allocated API keys. API Umbrella admins can view and modify these. Each user account in API Umbrella has an associated API key. 

## 2. Sources and further reading

https://api-umbrella.readthedocs.io/en/latest/developer/architecture.html