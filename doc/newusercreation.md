The following sequence diagram shows the flow of new user account creation and approval.

![](newusercreation.png)