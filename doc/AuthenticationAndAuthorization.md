# Authentication and Authorization

## 1. General

Authorization is the function of specifying access rights/privileges to resources, which is related to information security and computer security in general and to access control in particular.

Authentication is the act of proving an assertion, such as the identity of a computer system user. In contrast with identification, the act of indicating a person or thing's identity, authentication is the process of verifying that identity.

This documentation is limited to describe only possible end user Authorization and Authentication.

## 2. Authorization in Platform

Not all components provide native access control mechanisms. Umbrella proxy can be configured to place additional security in form of access control configuration.
The components intended for external usage have been connected via Umbrella proxy.   

Users can be authorized via following mechanisms:
* Bearer tokens
* HTTP Headers
* X-API-Keys (not utilised at the time of writing)

Bearer tokens are used to control the access to Tenants via Context Broker NGSI-V2 API. Bearer tokens are exactly what they say; Who ever carries (bears) this token may present it to a party which can inspect it. The Bearer Token is created by the Authentication server (Keyrock). It is not random but it is opaque, meaning that the token carries no information about the identity of the bearer. 

HTTP Headers (Authorization is also an HTTP Header, but used for Bearer Tokens) are essentially shared secrets. Both communication parties need to know these. These do not expire and need to be manually changed.

X-API-keys are in a way blend of Bearer Tokens and HTTP Headers. They are managed by the API Management. They are not utilised at the moment.

Authorization takes place in the Umbrella proxy. Requests are evaluated at the proxy (Policy Enforcement Point) and either accepted or rejected.

### 2.1 Authorization mechanisms by components:

| Component | Mechanism |
| ------ | ------ |
| Context Broker | Bearer token |
| Cosmos/flink | HTTP header |
| Perseo | HTTP header |

## 3. Authentication in Platform

Authentication takes place in a form of a UI login or API access. Users provide their credentials to log in. End users can (given they have been granted an access) login to:
* User Management 
* API Management (Keyrock login)
* Knowage (Keyrock login)

Admin login is a separate topic not discussed here.

## 4. Tenant Authorization flow

In order to access a Tenant, user must present a valid Bearer token. Token is passed as `Authorization` within an HTTP Header.
Umbrella will extract the Token and send it to Keyrock for introspection.
Keyrock will check for Token validity and pass information back to Umbrella.
Umbrella will then either Deny or Allow the request. In case of Deny, User is presented with the HTTP-Response-Code `403 Unauthorized`.
In case Allow, request is passed to the Context broker, which will process the request and send a response back to Umbrella.
Umbrella will pass the Response as an answers to the end user's request.

## 5. Sources and further reading

https://en.wikipedia.org/wiki/Authentication

https://en.wikipedia.org/wiki/Authorization