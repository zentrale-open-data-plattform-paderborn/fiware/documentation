# Overall Architecture / structure

General Information about the Components can be found in the [FIWARE Catalogue](https://www.fiware.org/developers/catalogue/)

![Screen-Shot-2018-12-19-at-09.20.05.png](/images/fiware1.png)

FIWARE is a collection of components that can be combined together. A FIWARE setup was realised in this project. This integrations connects the FIWARE components to allow realisation of data driven user cases.

This deployment touches all the areas of FIWARE reference architecture, but is not utilizing all the available components from the FIWARE component catalogue. The project can and should be extended as requirements and needs arise.

FIWARE is also a ever moving and developing landscape; new components arise and some are depricated.

The list of components can be found on [this page](component%20list.md). 

# High level component overview 

![component diagram](/images/dbaccessview.PNG)