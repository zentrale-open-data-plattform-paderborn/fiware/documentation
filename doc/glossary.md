# Glossary
**Branch** A Gitlab concept of making a copy of a repository within the repository.

**CB** Acronym for Context Broker.

**Cluster** A synonym for a container-based infrastructure.

**Gitlab** A versioning system with build capabilities.

**Gitlab Variable** A place to store values within a project but outside of the repository.

**K8s** Acronym for Kubernetes.

**Namespace** A concept to encapsulate resources within a Kubernetes cluster.

**Pipeline** A gitlab based term representing the automatic build/deployment process. Defined in the file _.gitlab-ci.yml_ with a repository. 

**Pod** A Running container (or even more than one container) in kubernetes.

**QL** Acronym for QuantumLeap

**Stage** A definition of software components running in a namespace on a cluster.

**Tenant** A Tenant encapsulates context data from each other (including rights management).

