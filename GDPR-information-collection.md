DRAFT

When user is created, user information for email creation is recorded in Keyrock. When user logs into API Management platform, a user (which Keyrock is not aware) is created. Thus removing account (by end user or Admin) form Keyrock does not remove user from API management platform. This needs to be handled manually by API management platform admin.

The collected information (email) is only used to maintain accounts and identify via username in applications.

Recommendation is to update the keyrock signup link and mention there everything to do with the GDPR.

**List where users can put data:**

In API Management:

- Tenant Description field

- In New API creation (can be restricted and should be to Admin)

- Users can see each other usernames (not emails)

- Admin can see all for admin purposes

- In creating organisations (can be restricted and should be to Admin)

- Contact form

In Keyrock:

- Users can create applications. This should be discouraged, but if the user set is known and approved, this should be fine.

- Data in description fields, etc.

- Users can see each other's usernames (not emails)

- Admin can see all for admin purposes

In addition to this, the API Management has it's own Privacy Policy and Terms and conditions, and they should align with the Keyrock signup link.

Context Broker:

- Theoretically it is possible to push any data to the Context Broker. If the user set is known and approved, this should be not problem.

Knowage:

- If 3rd party users are allowed to access Knowage, they can have different roles. Depending on the role they can input information to various fields. Please see [Knowage documenation](https://knowage.readthedocs.io/en/latest/) for further description.

Umbrella:

- Only admins should be allowed to Umbrella


Other components should not be configured to collect any information, and current documenation does not instruct so.