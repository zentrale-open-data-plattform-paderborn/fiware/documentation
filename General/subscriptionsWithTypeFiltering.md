## Subscriptions

Subscriptions can be used to filter data. This can be used to select what data goes where. There are two options: filter by ID or filter by Type.

### 1. Filtering by ID example:

```
{
        "description": "Conditional subscription",
        "subject": {
          "entities": [{ "idPattern": "id1." }],
          "condition": { "attrs": [] }
        },
        "notification": {
          "attrs": [],
          "http": { "url": "http://quantumleap.fiware-prod.svc.cluster.local:8668/v2/notify" },
          "metadata": ["dateCreated", "dateModified", "timestamp"]
        }
      }
```
This subscription defines `idPattern`, which is a regex. In the example, if there is an entity or entities with ID which has `id1` string in the id and that/those is/are updated, a notification is sent to QuantumLeap. If there is an update to entity which Id does NOT contain string `id1`, update is not done.

For comparison:

Entity with ID: `id1:part2:3` will cause an notification to be sent to QuantumLeap
Entity with ID: `id2:part2:3` will NOT cause an notification to be sent to QuantumLeap

There can be several subscriptions at the same time active. 

### 2. Filtering by type example: 

Let's say there is a type `cityIotData`. If we’d have another type, say `cityIotData2` we can filter with that type. With a subscription like this:
```
      {
        "description": "subtest1 subconditinal1",
        "subject": {
          "entities": [{ "idPattern": ".*", "type": "cityIotData" } ],
          "condition": { "attrs": [] }
        },
        "notification": {
          "attrs": [],
          "http": { "url": "http://quantumleap.fiware-prod.svc.cluster.local:8668/v2/notify" },
          "metadata": ["dateCreated", "dateModified", "timestamp"]
        }
      }
```
data like this will NOT cause a notification, since it’s type `cityIotData2`:

```
{
    "id": "id1:part2:3",
    "type": "cityIotData2",
    "dateObserved": {
        "type": "DateTime",
        "value": "2020-07-02T09:40:43+00:00"
    },
    "location": {
       "type": "geo:json",
       "value": {
          "type": "Point",
          "coordinates": [2.3,4.5]           
       }
    },
    "app_id": {
        "value": "app_id4"
    }

}
```

Combination of both Type and Id  can be used. The `condition` in the subscription is used to control under what conditions the notification is being sent, but all the events are needed in reality. Notification `attrs` can be used to filter if all the data is not needed. 

### 3. Splitting example based on the type

This will push all data typed `cityIotData` to a new Tenant `optest2` that exists only in QuantumLeap:

```
curl --location --request POST 'https://context.fiware.opendata-city.de/v2/subscriptions' \
--header 'Authorization: Bearer 868b40e20fefa8f6f58bab54d6ebf070721febb2' \
--header 'Content-Type: application/json' \
--header 'fiware-service: optest1' \
--data-raw '      {
        "description": "subtest1 opnotify optest2",
        "subject": {
          "entities": [{ "idPattern": ".*", "type": "cityIotData" } ],
          "condition": { "attrs": [] }
        },
        "notification": {
          "attrs": [],
          "httpCustom": {
      "url": "http://quantumleap.fiware-prod.svc.cluster.local:8668/v2/notify",
      "headers": {"fiware-service": "optest2"}
      },
          "metadata": ["dateCreated", "dateModified", "timestamp"]
        }
      }'
```

Any other filtering can be performed to split data to a separate database. This is also outside the Tenant system, and there is no such Tenant in Context Broker.

There is a scenario where you can notify another Context Broker entity. If there is a subscription like this:

```
      {
        "description": "subtest1 opnotify",
        "subject": {
          "entities": [{ "idPattern": ".*", "type": "cityIotData" } ],
          "condition": { "attrs": [] }
        },
        "notification": {
          "attrs": [],
          "httpCustom": {
            "url": "http://orion.fiware-prod.svc.cluster.local:1026/v2/op/notify",
            "headers": {"fiware-service": "optest1"}
      }          
        }
      }
```

it will create a new Tenant to Context Broker. This Tenant is outside Tenant system, so in order to access this Tenant, a Tenant named `optest1` needs to be created in APInf platform. Then in turn a subscription like this can be done:

```
curl --location --request POST 'https://context.fiware-prod.opendata-city.de/v2/subscriptions' \
--header 'Authorization: Bearer 868b40e20fefa8f6f58bab54d6ebf070721febb2' \
--header 'Content-Type: application/json' \
--header 'fiware-service: optest1' \
--data-raw '{
        "description": "subtest1 subconditinal1",
        "subject": {
          "entities": [{ "idPattern": ".*", "type": "cityIotData" } ],
          "condition": { "attrs": [] }
        },
        "notification": {
          "attrs": [],
          "http": { "url": "http://quantumleap.fiware-prod.svc.cluster.local:8668/v2/notify" },
          "metadata": ["dateCreated", "dateModified", "timestamp"]
        }
      }
'
```
which will notify QuantumLeap. If data is now pushed to original tenant (A), it goes to another tenant(B) and then from there to QuantumLeap:

```
+----------------+            +--------------+                +-------------+
|                |  OP Notify |              |   Notify       |             |
|  CB            | +--------->+  CB          +--------------->+  QL         |
|  Tenant A      |            |  Tenant B    |                |             |
|                |            |              |                |             |
|                |            |              |                |             |
|                |            |              |                |             |
+----------------+            +--------------+                +-------------+
```

More on subscriptions: https://fiware-orion.readthedocs.io/en/master/user/walkthrough_apiv2/index.html#subscriptions
