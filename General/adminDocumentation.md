# Admin Documentation

This documentation outlines the most essential tasks Admin needs to take. It is recommended to read through earlier documents in the repository to gain high level understanding of the documentation. When additional information on particular component is needed, please refer to the components native documentation.
Components are listed in the [Component List](/doc/component%20list.md) page.

## 1. Keyrock approvals

System is configured so that signup emails are diverted to admin email. Vanilla Keyrock does not allow for such configuration. In order to accomodate for such Admin approval work flow,
`controllers/web/users.js` and few other files are replaced in CI pipeline (copy in Dockerfile) . Please see the Keyrock gitlab project for details.

There is a default keyrock admin. Either use this for approvals or create a new account and use this to do the approvals. After doing an approval in Keyrock (users->enable), an email is sent to the person who's account has been enabled. This email should contain instructions and such.

In similar fashion, Admin can change user passwords, disable and remove them.

For detailed instructions, please refer to [New User Onboarding-documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation/-/blob/documentation/howto/new%20user%20onboarding.md) 

## 2. Tenant promotions & cleanup

For users to be able to create Tenants, they need to be added to the API Catalogue application in Keyrock (Authorized users list) and have a "tenant-manager" role in the API Catalogue application. Revoking this role will prevent users from modifying the Tenants. The previously created Tenants will remain in the Platform. If needed, these Tenants need to be cleaned manually from Keyrock, Tenant-Manager (mongo db) and API Umbrella:

* Keyrock: Remove the organisation (matches with Tenant name)

* Tenant-manager: Go to Mongo DB and remove the Tenant from Tenants collection

* API Umbrella: Find the Sub URL rule from the Orion Context broker API and remove that.\
  Effectively removing the sub url entry from Umbrella will prevent the API access.  

One recommended option is that if the user is banned or blacklisted and Tenant removal is needed, Admin changes the user's password, logs into API management and removes the Tenants or modifies user lists. After this, user can be removed or disabled.

There is no view for Tenants for Admin to see all the Tenants directly. Manually removing Tenants is not recommended.

## 3. Adding and Removing APIs

Only API Management Admin can add and remove APIs. This is configurable in the platform setting by API Management Admin. Adding APIs is described in [installation configuration instructions](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/platform-setup/README.md#435-configure-apis).

It is important not to touch once set up Orion Context Broker and QuantumLeap API network setting when once set in API management. If done so, it will wipe sub url settings and stop Tenants from working.

Proxy order is not to be changed, as Tenant-manager relies on the fact that Orion Proxy is the first.

## 4. Component Configuration Overview

### 4.1 Knowage Configuration and Operations

Please refer to [Knowage configuration](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/howtos/KnowageConfig.md)

### 4.2 Perseo Configuration and Operations

Please refer to [Perseo configuration](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/howtos/PerseoConfig.md)

### 4.3 Cosmos Configuration and Operations

Please refer to [Cosmos configuration](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/howtos/CosmosConfig.md)

### 4.4. Kurento Configuration and Operations

Please refer to [Kurento configuration](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation/-/blob/documentation/General/Kurento.md)

### 4.5 Tenant-Manager Configuration and Operations

The Tenant Manager is a middleware component which allows managing Tenants. This includes lifecycle and user management for Tenants. Tenant manager persists information about Tenants in Mongo DB. Tenant manager utilises a configuration file `credentials.json`. If any of the these change (Keyrock admin details, Catalog app id, Market app id) the credentials.json needs to be updated and Tenant-Manager needs to be re-deployed (see [platform testing documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/documentation/platform-setup#5-install-tenant-manager) for more details.). Tenant manager accesses and modifies SUB-URL rules in API umbrella. It also accesses and modifies Keyrock Organisations as needed. 

### 4.6 API Umbrella Configurations and QuantumLeap

If access to QuantumLeap is needed, it is necessary to create a sub url rule to QuantumLeap API in umbrella. This is described in [platform testing documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/documentation/platform-testing#9-historical-data-acquisition-quantumleap). No other actions are be needed. At the moment QuantumLeap does not recommend publishing the API due to security issue. If access to QuantumLeap is needed, it's configuration is described in [Retrieving Data from QuantumLeap-howto](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation/-/blob/documentation/howto/retrievingDataFromQuantumLeap.md).

### 4.7 Data to QuantumLeap

If data is needed to be persisted in QuantumLeap, a subscription needs to be created. This is described in [platform testing documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/documentation/platform-testing#53-historical-data-handling) and as a [how-to](howto/creatingSubscriptions.md). No other actions are be needed.

### 4.8 Visualization via Grafana

This is described in [platform testing documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/documentation/platform-testing#10-grafana), but standard Grafana configuration options apply.

### 4.9 X-API-Key vs. Bearer Tokens

X-API-keys and Bearer Tokens are two ways used to control access to the APIs added to the Platfrom. They work in slightly different manner and more details are provided in the [Authentication And Authorization](../doc/AuthenticationAndAuthorization.md) document.

X-API-Keys (Api keys) can be used to access APIs added to the API management. Api keys allow proxy wide access, unless otherwise configured. Tokens are to be used with Tenants and Context Broker API exclusively, they cannot be used with other APIs.
This can be configured differently (directly in the Umbrella by adding required roles to sub url rules), but it's manual work and thus fragile and error prone. This approach is not recommended.

## 5. Security

On the Platform, only port 443 is configured. All the traffic is routed via API proxy. IP addresses are masqueraded.

The external access to the services is handled via TLS (only HTTPS connections are allowed).

For fetching data from Tenant, Oauth2 tokens are used. X-api-key can be also used, but they should be used by trusted applications only. If X-API-Keys need to be used, Umbrella configuration needs to be changed to support this. Current intallation allows only Bearer tokens.

QuantumLeap has a SQL injection vulnerability at the tie of writing and we cannot recommend the access to be open. <https://github.com/smartsdk/ngsi-timeseries-api/issues/332>

Tenant-manager and Keyrock have secrets (user accounts and umbrella tokens) in plain text on the docker. Implications are that is someone gains access to the system, they can use these to falsify information.

Access to Umbrella front page  is allowed, but getting API keys is disabled via API Umbrella config setup time. This configuration prevents unknown (i.e. no Keyrock account and approval) users from accessing the system. 

## 6. Subscriptions

Handling Context Broker subscriptions are allowed for users with a valid Bearer token. 
Valid Bearer tokens can be fetched only by users that have an approved Keyrock access. 
X-API-keys can be also used to handle subscriptions.
Handling subscriptions include querying (GET), creating (POST), modifying (PATCH) and deleting (DELETE) subscriptions. 
How to do subscriptions is covered under how [Creating Subscriptions](../howto/creatingSubscriptions.md).


## 7. Known issues

* QuantumLeap has an SQL injection vulnerability <https://github.com/smartsdk/ngsi-timeseries-api/issues/332>

* API Management: <https://github.com/Profirator/Profi-platform/issues>
 
## 8. Technical notes

Modifying database context directly (drop table, for example) may have unseen side effects, as this may cause orphan data artifacts to the system. A thorough test in Dev or QA stage is recommended before attempting such operations in Production.
