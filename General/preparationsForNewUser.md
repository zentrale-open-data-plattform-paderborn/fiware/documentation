# Preparation for new users

Needed steps when a new a new user requests access to the system in the demodata scenario.

## 1. Preparations

1. `demodata` Tenant is created by keyrock admim
2. Some data is available at the Tenant
 
## 2. Steps to enable user and allow the user to read Tenant data:

1. Login with admin to [Keyrock](accounts.fiware.opendata-CITY.de)
2. Under [Users](https://accounts.fiware.opendata-CITY.de/idm/admins/list_users) enable the new user
3. Send email to the user saing that they have been approved.
4. Go to [API Management](https://apis.fiware.opendata-CITY.de/tenants/) and edit the `demodata` Tenant. Add the new user with `data-consumer` role.