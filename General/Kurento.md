# Kurento Configuration (Current state)

Kurento can de deployed via Gitlab CI/CD, please refer to the [project](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/kurento). Deployment and deployment tests are described in Installation project.

Current state is that project (version 6.8.0) is deployed, but no configurations are done.

For further information please refer to [Kurento documentation](https://kurento.readthedocs.io/en/stable/)

The Stream Oriented Generic Enabler (GE) provides a framework devoted to simplify the development of complex interactive multimedia applications through a rich family of APIs and toolboxes. It provides a media server and a set of client APIs making simple the development of advanced video applications for WWW and smartphone platforms. The Stream Oriented GE features include group communications, transcoding, recording, mixing, broadcasting and routing of audiovisual flows. It also provides advanced media processing capabilities involving computer vision, video indexing, augmented reality and speech analysis.

Development (Client connecting to Kurento backend) and additional infrastruture (video source) is required to utilize Kurento. Once a Kurento Media Server is installed, you need a Kurento Client to create your own applications with advanced media capabilities. A Kurento Client is a programming library used to control the Kurento Media Server from an application. Communication between a Kurento Client and the Kurento Media Server is implemented by the Stream Oriented GE Open API. This communication beetween Kurento Clients and Kurento Media Server is done by means of a WebSocket.

