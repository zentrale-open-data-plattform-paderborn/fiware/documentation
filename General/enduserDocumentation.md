# End user Documentation

This document is intended for the end users of the FIWARE IoT platform and API Management.\
End users are mainly developers, using FIWARE in order to connect to the platform either pushing data and retrieving data respectively.\
It briefly describes the system, key concepts and kickstarts users in getting data out of the platform for quick experimentation.

If you are looking for Admin documentation it's [here](adminDocumentation.md).

## 1. TL;DR

Go to User Management (also known as [Keyrock](https://fiware-idm.readthedocs.io/en/latest/)) in order to sign up a new user. Wait for email confirmation stating that your account has been activated. Go to API-Management. Login with FIWARE using the credentials you created before and navigate to Tenants. In the list you should see `demodata` Tenant entry. Click Authorization. Enter password you created before to get a Bearer token.

Download [this](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/025ffee31a1cc0a809dd5080d7b878e10a59cdba/platform-testing/collections/SCP_Documentation.postman_collection.json) Postman collection. Insert the retrieved Bearer token. Hit Send. See the data. You are done.


## 2. Platform Features

* Right time data storage in Context Broker for serving and receiving NGSI-V2 data.
* API Management and API documentation for API usage.
* Unified user management via Keyrock IDM.

## 3. What is FIWARE?

**FIWARE is an open source initiative** defining a universal set of standards for **context data management** which facilitate the development of Smart Solutions for different domains such as Smart Cities, Smart Industry, Smart Agrifood, and Smart Energy.

**In any smart solution there is a need to gather and manage context information**, processing that information and informing external actors, enabling them to actuate and therefore alter or enrich the current context. The **FIWARE Context Broker** component is the core component of any “Powered by FIWARE” platform. It enables the system to perform updates and access to the current state of context.

**The Context Broker in turn is surrounded by a suite of additional platform components**, which may be supplying context data (from diverse sources such as a CRM system, social networks, mobile apps or IoT sensors for example), supporting processing, analysis and visualization of data or bringing support to data access control, publication or monetization.

More on <https://www.fiware.org/developers/>


## 4. Available Data

There are various data in the platform but the platform Administrator controls access to the data. Data is divided into two categories; `IoT data` and data from `API`s. The term "data" means JSON based data fetched using API calls.


### 4.1 NGSI Data models and why this is important

Before pushing data to context broker, please read the basics of NGSI: <https://www.fiware.org/developers/data-models/>

Using standard data models are important for cross functionality and interoperability of the platforms. Think of write one app use anywhere. If the data model is the same, no adaptations are necessary.

### 4.2 Tenant System in Brief

Tenant is a [Context Broker](https://fiware-orion.readthedocs.io/en/master/index.html) concept. It means logical data separation. Different Tenants can reside on the same Context Broker without knowing and interfering each other. Technically this means that Tenants have one-to-one "fiware-service" - "Tenant" mapping; one Tenant is used to control access to fiware service. Although it is possible to put what ever data in one Tenant, it's a good idea to keep one-to-one Tenant Data model mapping. 

The one-to-one mapping means that what ever is the Tenant name (for example demodata) is the same as the Fiware Service. So in order to access data on the "demodata" Tenant, you need to set the fiware service header value to "demodata":

```
fiware-service : demodata
```

Tenant owner needs to authorize users to the Tenants; If you do not have authorization, you don't have the visibility to the Tenants.

To allow user to add and modify Tenants, Platform Administrator needs to grant "tenant-admin" role to the user.

### 4.3 How to Get Data from the Platform

After you have created an account in User Management and received the confirmation email you can login to the API-Management. You need to login using the account created before - this is the account to all platform services.

There are two data sources. APIs and Tenants. To see all the APIs, navigate API-Management. Select one of the APIs to start exploring. Under Endpoint tab users can see Open API Spec documentation of the API and possibly other resources.

After fetching the API key, see Overview tab for API key usage.

To see all the Tenants you have access to, navigate to Tenants. You see a list of Tenants and authorized users. Individual Tenants have a Read (data-consumer)/write (data-provider) granularity. You will need an OAuth2 Bearer token to access the data in Tenants.

### 4.4 Tokens and Refresh mechanism

Bearer tokens are used to enforce access control over Tenants. There are two ways to get tokens, manually from API-Management, or via API call, which is described below section "Bearer Token retrieval". It is also documented [here](https://apinf-fiware.readthedocs.io/en/latest/#tenant-manager-ui) under section "RETRIEVING A TOKEN PROGRAMMATICALLY". In any case, it is useful to download this [Postman collection](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/ab8a0af3012e37591e80e0ed4f777c33aebf1181/platform-testing/collections/SCP_Documentation.postman_collection.json).

#### 4.4.1 Bearer Token retrieval

1. Go to User Management.
1. Sign in, if you are not already signed in.
1. On the left, select Applications and then select the API Catalogue from the List.
5. You should find an entry **OAuth2 Credentials**. Click the dropdown icon on the right of it.
1. Copy both Client ID and Client Secret to an editor. We'll need them later on.
1. Switch to Postman and open the Request "SCP Documentation -> Keyrock -> POST: Get Authtoken"
1. Go to the Authorization Tab, copy the Client ID to **Username** and Client Secret to **Password**
   > ***NOTE:*** ClientID and Secret will never be transmitted in plain text.
1. Next go to the Body Tab and insert username and password.
   > ***NOTE:*** Although the requests ask for the username, only the email will work.
1. Press `Send`, to send the request to Keyrock.

* Expected: The response's body should look like this:

```
{
    "access_token": "734c1731318e8ab30ccba431b31faaf4a20c4bb9",
    "token_type": "Bearer",
    "expires_in": 3599,
    "refresh_token": "5a054b2d1814f27aa4d3d248f28335e532719149",
    "scope": [
        "bearer"
    ]
}
```

#### 4.4.2 Refresh Token

With every request of a bearer token, you get the bearer token (field: access_token) and a second token (field: refresh_token), that can be used to refresh your access token to extend the time, since the bearer token will expire eventually (given in seconds in field: expires_in).

To refresh the token, we just need to send the refresh token together with the Basic Authentication to Keyrock.

1. In Postman open the Request "SCP Documentation -> Keyrock -> POST: Refresh Token"
2. Again, go to the Authorization Tab, copy the Client ID to **Username** and Client Secret to **Password**
3. Go to the Body tab, copy the refresh_token from the previous step and insert it to the key `refresh_token`
4. Press `Send`, to send the request to Keyrock.
5. Expected: The response's body should look like this:

```
{
    "access_token": "efb0dc68f8a04c1d51891be1b95492a41c075fc9",
    "token_type": "Bearer",
    "expires_in": 3599,
    "refresh_token": "8902cac3c2b52619cbc95d8b03384ae0f96f89c4",
    "scope": [
        "bearer"
    ]
}
```
### 4.5 How to Push Data to Platform - Add Tenant

In order to push data to the platform (i.e. Context Broker) the user that is used to push, needs to have tenant-admin role (see User Management). Platform Admin needs to grant this role. To gain approval, send an email to EMAIL_OR_CONTACT_DETAIL_GOES_HERE with topic "Tenant creation request".

After you have received approval confirmation, re login to the 
API-Management. Navigate to Tenants.

1. Enter the Tenant name and a description.
2. Click `Add Tenant` to save.

The Tenant should be created with no errors and be listed.

3. While we are here, Switch to the `Authorization` Tab, enter your password and select `Get New Token`.
4. Copy the access token.
5. Switch to Postman and open the Request "SCP Documentation -> Orion -> GET: TestEntities"
6. In the Headers Tab, insert your bearer token.
7. Modify / replace the Fiware-service header value to the the Tenant name you gave in step one.
8. Press `Send`, to send the request to Orion Context Broker.

* Expected: The response status code should be `200 OK` and the body should be empty:

```
[]
```

This confirms that the Tenant is created. You are now ready to push data to that Tenant. As a Tenant creator, you have implicit write access.

The following demonstrates how to push data to Context Broker using Postman. Once you have been authorized to add Tenants, you can remove old ones and add new ones at will. Please note that if you remove the Tenant, the data is not removed from Context Broker. You need to ask for data removal by sending email to EMAIL_OR_CONTACT_DETAIL_GOES_HERE with topic "Tenant Data Removal" and wait for further communication.

Post some data towards the Orion Context Broker:

1. In Postman open the Request "SCP Documentation -> Orion -> POST: TestData"
2. In the Headers Tab, insert your bearer token.
3. In the Headers Tab modify the Fiware-service header to match the Tenant you want to push the data into.

You can take a look at the body. There is the data, that will be sent. You can do changes on the values, if wanted.

4. Press `Send`, to send the request to orion.

* Expected: The response status code should be `204 No Content` and should have no body.

For a more detailed description on this process, please refer to [Testing Documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/documentation/platform-testing/README.md#5-manual-data-commit).

Now we can read that data from context broker again.

5. In Postman open the Request "SCP Documentation -> Orion -> GET: TestEntities"
   > ***NOTE:*** This it is the same as before, so you could just resend that one, if you did not close the tab.
5. In the Headers Tab, insert your bearer token.
6. In the Headers Tab modify the Fiware-service header to match the Tenant you want to read values from.
7. Press `Send`, to send the request to orion.

* Expected: The response status code should be `200 OK` and the body should be like:

```
[
    {
        "id": "first",
        "type": "Thing",
        "precipitation": {
            "type": "Number",
            "value": 0,
            "metadata": {}
        },
        "relativeHumidity": {
            "type": "Number",
            "value": 122,
            "metadata": {}
        }
    }
]
```

### 4.6 Add users to Tenants

To invite users to Tenant, click Edit and select user(s) from list. Give them the read / write access and choose if they get email notification. Click "Add user for Tenant" for each. Remember to click "Modify tenant".\
See an example [here](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/documentation/platform-testing#32-tenant-functionality).

### 4.7 Adding a New API

Only Platform Admins can add new APIs. Contact EMAIL_OR_CONTACT_DETAIL_GOES_HERE if you need your API to be added.

## 5. GDPR

Please note that if you remove the Tenant, the data is not removed from Context Broker. You need to ask for data removal by sending email to EMAIL_OR_CONTACT_DETAIL_GOES_HERE with topic "Tenant Data Removal" and wait for further communication.

When a user is created, this process is recorded in User Management. When user logs into API Management, a user is created within API-Management (which User Management is not aware of). Thus, removing accounts from User Management does **not** remove the corresponding user from API-Management.

The collected information (email) is only used to maintain accounts and identify via username in applications.

IP adresses are masqueraded.

If you have any questions regarding GDPR, please reach out to EMAIL_OR_CONTACT_DETAIL_GOES_HERE


## 6. X-API-Key vs. Tokens

X-API-Keys (Api keys) can be used to access APIs. Api keys allow proxy wide access, unless otherwise configured. Tokens are to be used with Tenants and Context Broker API exclusively, they cannot be used with other APIs.

## 7. Security

On the server, only port 443 is configured. All the traffic is routed via API proxy. IP adresses are masqueraded.

The access to the services is handled via TLS (only HTTPS connections are allowed).

For fetching data from Tenant, OAuth2 tokens are used. X-API-keys can be also used, but they should be used by trusted applications only.

## 8. Further Reading

<https://www.fiware.org/developers/>

<https://www.fiware.org/developers/data-models/>

## 9. Who to Contact

Primary contact in all cases is EMAIL_OR_CONTACT_DETAIL_GOES_HERE