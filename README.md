# Documentation

This project contains the documentation of the FIWARE/API-Management software stack.

1. General
    1. [Preface](doc/preface.md)
    1. [Cluster Overview](doc/cluster.md)
    1. [Project Structure](doc/project structure.md)
    1. [Gitlab Branching Strategy and Naming Convention](doc/gitlab.md)
    1. [Deployment](doc/deployment.md)
1. Architecture
    1. [Overview](doc/Architecture Overview.md)
    1. [Components List](doc/component list.md)
    1. [Dependency Graph](doc/Dependency Graph.md)
    1. [Backend Overview](doc/Backends Overview.md)
    1. Authentication/Authorization
        1. [Authorization](doc/AuthenticationAndAuthorization.md)
        1. [Policy Enforcement (APInf/Tenant Manager)](doc/Policy%20Enforcement.md)
    2. UML sequence diagrams
        1. [New User Approval](doc/newusercreation.md)
        2. [Tenant Creation](doc/tenantcreation.md)
        1. [Data POST](doc/datapost.md)
        1. [Data GET](doc/dataget.md)
1. [Adminstration Documentation](General/adminDocumentation.md)
    1. [Subscriptions With Type Filtering](General/subscriptionsWithTypeFiltering.md)  
1. [Enduser Documentation](General/enduserDocumentation.md)
1. How To's
    1. [New User Onboarding](howto/new user onboarding.md)
    1. [Creating Subscriptions](howto/creatingSubscriptions.md)
    1. [Retrieving Data From QuantumLeap](howto/retrievingDataFromQuantumLeap.md)
1. Appendices
    1. [Glossary](doc/glossary.md)
    1. [Authors](doc/Authors.md)


## Funding Information:
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](images/logoleiste.JPG)

## License:
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh. This work is licensed under a CC BY SA 4.0 [license](https://creativecommons.org/licenses/by-sa/4.0/). 
