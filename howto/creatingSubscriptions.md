# Creating Subscriptions from Orion Context Broker to QuantumLeap
## 1. Overview
Orion Context Broker has another powerful feature that you can take advantage of: the ability to subscribe to context information so when "something" happens (we will explain the different cases for that "something"), your application will get an asynchronous notification. This way, you don't need to continuously repeat query requests (i.e. polling). The Orion Context Broker will let you know the information when it arrives.

In this deployment subscriptions are used to get data from Orion Context Broker to QuantumLeap for historical data storage.

## 2. Prerequisites
1. Postman has to be installed on your device.
2. This Postman [collection](https://gitlab.opendata-city.de/fiware/installation/-/blob/documentation/platform-testing/collections/SCP_Documentation.postman_collection.json) is imported to Postman.
3. You have a valid User Management (Keyrock) account
4. You are authorized to add Tenants (Keyrock admin has given you tenant-admin role)

If you are not familiar with the subject, it is recommended to read through this document once before starting to familiarize yourself with the steps.

## 3. Creating a Subscription

Subscriptions are created per `fiware-service`, so the name of the FIWARE-service needs to be present in the Header.  This is the same value as the Tenant. For more information on subscriptions, please refer to chapter 3, Further Documentation.

### 3.1 Create a Tenant

Navigate to API-Management and sign in using FIWARE giving the Keyrock user credential:

![fiware](../images/signin-FIWARE.PNG)

Navigate to API-Management/Tenants and create a new Tenant:

![](../images/add-tenant1.PNG)

![](../images/add-tenant2.PNG)


### 3.2 Post data to the Tenant

It's important to either use normalised data models or keyvalue datamodels and not to mix them. Using `options=keyValues` can create problems in the historical data if used with location. See https://github.com/FIWARE/data-models/issues/576 for further discussion.

To post data to the Tenant, you need a valid access token:

Then you need to POST data to the Tenant.

1. In Postman open the Request "SCP Documentation -> Orion -> POST: TestData"
2. In the Headers Tab, insert your bearer token.
3. When creating the Tenant, you gave it a name. Change the fiware-service header in Headers tab to match the value. 

You can take a look at the body. There is the data, that will be sent. You can do changes on the values, if wanted.

3. Press `Send`, to send the request to orion.
* Expected: The response status code should be `201 Created` and should have no body.

### 3.3 Subscribe

Now we create a subscription, send some data and try to access the historical data.

1. In Postman open the Request "SCP Documentation -> Orion -> POST: TestSubscribe"
2. In the Headers Tab, insert your bearer token.
3. When creating the Tenant, you gave it a name. Change the fiware-service header in Headers tab to match the value. 

Take a look at the body, to see the content of the subscription. Make sure the URL matches the stage (e.g. fiware-dev for dev or fiware-prod to production)

3. Press `Send`, to send the request to orion.
* Expected: The response status code should be `201 Created` and should have no body.

QuantumLeap is now ready to receive historical data. 

To check the state of the subscription, you can do a GET to the Orion Context Broker v2/subscriptions end point:

![](../images/postman-get-subscription.PNG)


## 4. Further Documentation

API reference: https://fiware.github.io/specifications/ngsiv2/stable/ - has documentation about the subscription endpoint

QuantumLeap: https://quantumleap.readthedocs.io/en/latest/ - discusses the QuantumLeap subscriptions

Discussion on datamodels / optios parameter: https://github.com/FIWARE/data-models/issues/576

Grafana chart creation: https://gitlab.opendata-city.de/fiware/installation/-/blob/documentation/platform-testing/README.md section 10, "Grafana"