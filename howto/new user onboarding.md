# Onboarding new Tenant users / new user creation

## 1. General

Users need to be managed in a centralized manner. This installation uses Keyrock IDM as a single place to handle user management. Accounts should be created and maintained in Keyrock. Tenant system relies on the fact that users have a Keyrock account.

Alternatively users can login to API management separately using native login. In this case then the access to Tenant system is not available. Login methods can be enabled and disabled by API Management Admin. 

### 1.1 Steps to sign up

Sign up at Keyrock IDM (you need to confirm the email address by clicking the appropriate link in the received "confirmation email").

> **_NOTE:_**  There is a known issue on using dots "." in user names: <https://github.com/Profirator/Profi-platform/issues/2>

**After** the email-address is confirmed, sign in at API Management using FIWARE login.

### 1.2 Context Broker URL

The Context Broker NGSI V2 API is exposed at ENPOINT_HERE.

### 1.3 Tenant Access & getting tokens

To get kick started, there is a postman collection used in the acceptance testing available here <https://gitlab.opendata-city.de/fiware/installation/-/tree/master/platform-testing/collections>

One way to familiarize yourself with the system is to read through the acceptance testing: <https://gitlab.opendata-city.de/fiware/installation/-/tree/master/platform-testing> it will basically run you trough all the steps that allow you to use the system. Some of the steps require Admin access and are not intended for regular users.

Accesses for data can be handled by using OAuth Bearer tokens. Once you have signed in, you can fetch a token from platform under Tenants->Authorization:

![images/get-token.PNG](../images/get-token.PNG)

This will allow you to access all the Tenants on the Context Broker you have access to. Please see [documentation](https://apinf-fiware.readthedocs.io/en/latest/#tenant-manager-ui) on how to add Tenants and general information about Tenants.

Tokens can also be fetched from API endpoint. Please see testing Acceptance testing documentation section 4.1 for this.

Tenant is a Context Broker concept which allows logical separation of data. Tenants are not aware of each other and cannot share each other's data. 

Tenants have one-to-one FIWARE-service/Tenant mapping, i.e.: **One** Tenant is used to control access to **one** FIWARE-service. Although it is possible to put what ever data in one Tenant, it's a good idea to keep one-to-one Tenant- Datamodel mapping.

The one-to-one mapping means that what ever is the Tenant name (for example test1) is the same as the Fiware Service. So in order to access data on the "test1" Tenant, you need to set the fiware service header value to "test1":

```
fiware-service : test1
```

### 1.4 Tenant authorisation

In order to create and modify Tenants, Keyrock IDM admin needs to promote you. It's like a on/off switch.

Tenant owner needs to authorize users to the Tenants; If you do not have authorization, you don't have the visibility to the Tenants. 