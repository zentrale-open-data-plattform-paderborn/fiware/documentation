This How-To document expects that you can control your Kubernetes namespace without a kubeconfig file.

To prevent harmful SQL-statements coming from Grafana interface, it is recommended to use a specific user with resitricted read-only privileges.

To run following commands, you need to first activate an Enterprise Edition of CrateDB. Example How-To can be found [here](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation/-/blob/master/howto/Using%20CrateDB%20license.md).  

Create new CrateDB user 'readuser' with password 'readpassword'  
`kubectl -n YOURNAMESPACE exec -it quantumleapcrate-0 -- crash -c "CREATE USER readuser WITH (password = 'readpassword')"`

Grant the user cluster-wide query privileges  
`kubectl -n YOURNAMESPACE exec -it quantumleapcrate-0 -- crash -c "GRANT DQL TO readuser"`

More on user privileges in the [official documentation](https://crate.io/docs/crate/reference/en/3.3/admin/privileges.html).

Once the user is created, we can use it in 3rd party components like grafana.

Setting up CrateDB as data-source in grafana, using the previously created 'readuser'
<div align="center">![img](/images/grafanareaduser.png)</div>