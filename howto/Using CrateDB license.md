This How-To document expects that you can control your Kubernetes namespace without a kubeconfig file.

Following command issues one of the CrateDB containers to set a license in its cluster. This license will be persistent over restarts of the cluster. Once command is succesfully run, the CrateDB cluster will be using a licensed edition.

`kubectl -n YOURNAMESPACE exec -it quantumleapcrate-0 -- crash -c "SET LICENSE 'YOUR_LICENSE_KEY'"`[]([[url](url)](url))

With enterprise edition, it is suggested that you create a read-only user for all 3rd party use-cases, like Grafana. You can learn more [here](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation/-/blob/master/howto/Grafana%20datasource%20user%20change.md).