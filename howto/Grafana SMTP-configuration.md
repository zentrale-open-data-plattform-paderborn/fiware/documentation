Confugring Grafana with SMTP-functionality.

This instruction uses commands that assume you have control of your namespace, without a kubeconfig file.

Grafana SMTP-credentials configured using Gitlab-variables that are then built into the container image.

Gitlab-variables in the Grafana project:  
`SMTPHOST_DEV`, `-STAGING`, `-PROD` - Variable for SMTP-host server, in format smtp.example.com:port.  
`SMTPUSER_DEV`, `-STAGING`, `-PROD` - Variable for SMTP-user for previously mentioned host, in format user@example.com.  
`SMTPPASS_DEV`, `-STAGING`, `-PROD` - Variable for SMTP-password for previously mentioned user.  

Once the Gitlab-variables has been changed, you need to re-deploy Grafana by first stopping current deployment and running the CI/CD pipeline for changes to take effect.

`kubectl delete statefulset grafana -n YOUR-NAMESPACE`

Run pipeline to deploy Grafana with new variables.