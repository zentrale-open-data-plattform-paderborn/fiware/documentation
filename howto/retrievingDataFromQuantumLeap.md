# Retrieving Data from QuantumLeap

## 1. How to retrieve data from QuantumLeap

This document described how to configure the system so data can be fetched from QuantumLeap securely.

### 1.1 Preface

Currently the access to the QuantumLeap API is restricted by additional header setting and not publicly accessibe. This is due to the fact that there is a vulnerability: https://github.com/smartsdk/ngsi-timeseries-api/issues/332

## 2. Umbrella settings

Umbrella settings need to be changed in order to gain secure access to Quantumleap.

### 2.1 Prerequisites

1. There is data in QuantumLeap and the Tenant name is known (effectively a subscription is created). 


### 2.2 Reconfiguring Umbrella for access

To test the access try QuantumLeap's /v2/version endpoint, this should be allowed for everyone.

Umbrella has an Sub-URL rule setting requiring a particular header to be set in order to allow data access. For security reasons, values are not exposed in this documentation. In order to find out the actual settings, go to Umbrella admin login and login with admin user. Then go to Umbrella's API backends, click on the Quantum Leap API, and click on the Sub-URL setting to reveal a list of the rules.

Currently Umbrella is configured in a way that no one can access data. To enable data access to limited amount of trusted users, do the following:

1. Remove all rules except `GET ^/v2/version$` and one of the `GET	^/`
2. Replace the Required Headers key and value with certain key-value pair and Disable API key checks on the remaining `GET	^/` rule:
![](../images/QL-data-access-sub-rule2.PNG)
3. Save.
4. Publish settings.


## 3. Fetching data example

1. Test the access with this curl:
```
curl --location --request GET 'https://sthdata.fiware.opendata-city.de/ql/v2/entities/INSERT_ENTITY_ID_HERE?limit=10' \
--header 'fiware-service: load1' \
--header 'supersecretvalue: notforyoureyes'
```

Expected is some data as response, there is a `limit` argument in the query in case there is a lot of data in the system.

If you do not know what entities are available for `INSERT_ENTITY_HERE`, it can be checked from the Context broker by asking like this:

```
curl --location --request GET 'https://context.fiware.opendata-city.de/v2/entities/?attrs=id' \
--header 'Authorization: Bearer 4f96d61796f8805fec4f6fe579c852f1fa8e12c6' \
--header 'fiware-service: load1'
```

The response will look like this:

```
{
        "id": "BMP180_11424",
        "type": "WeatherObserved"
    },
    {
        "id": "BMP180_3286",
        "type": "WeatherObserved"
    },
```


where the "id" is the entity id, so in this case a potential new query for the historical data from QuantumLeap would be:

```
curl --location --request GET 'https://sthdata.fiware.opendata-city.de/ql/v2/entities/BMP180_11424?limit=10' \
--header 'fiware-service: load1' \
--header 'supersecretvalue: notforyoureyes'
```



## 4. Further reading

QuantumLeap-"Read the Docs": https://quantumleap.readthedocs.io/en/latest/