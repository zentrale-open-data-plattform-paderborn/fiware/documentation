Installing Grafana plugins with Gitlab-variables.

This instruction uses commands that assume you have control of your namespace, without a kubeconfig file.

Gitlab-variable in the Grafana project (INSTALLPLUGINS_DEV, -STAGING, -PROD) is passed to deployment as an environment variable "GF_INSTALL_PLUGINS".
Desired plugins can be named in the variable, separated with comma symbol.
example variable to install worldmap-panel and imageit-panel:

`grafana-worldmap-panel,pierosavi-imageit-panel`

Once the Gitlab-variable has been changed, you need to re-deploy Grafana by first stopping current deployment and running the CI/CD pipeline for changes to take effect.

`kubectl delete statefulset grafana -n YOUR-NAMESPACE`

Run pipeline to deploy Grafana with new variable.

Official documentation on using plugins with a container deployment:
https://grafana.com/docs/grafana/latest/installation/docker/#install-plugins-in-the-docker-container