**Steps to resize Persistent Volume (PV) via editing Persistent Volume Claim (PVC)**

This document assumes you have connectivity and privileges to operate in the kubernetes cluster in your preferred namespace to do following changes.  
Please refer to this access documentation before continuing: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/documentation/-/blob/documentation/doc/cluster.md#2-accessing-the-cluster

Find Persistent Volume Claim name, by listing PVCs  
`kubectl get pvc`  

To start resizing Persistent Volume Claim, replace placeholder `pvc-name` with PVC-name of your choosing.  
`kubectl edit pvc 'pvc-name'`  

This command may default to using VIM text editor.  
Basic functionality of VIM:  
```
press 'i' to enter INSERT-mode. This allows you to edit text.  
press 'ESC' to exit INSERT-mode after done editing.  
write ':wq' to WRITE and QUIT. Make sure you exit the INSERT-mode before this step.  
```


In edit we can find the Persistent Volume Claim spec.

```
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
```

Find the requested storage size (in this example 2Gi) under Persistent Volume Claim spec and change to desired value.  

> Note: Lowering the value may cause problems and possible data loss!  

For changes to take effect restart the container for which the volume belongs to. Deleting the component pod will restart the component.  
`kubectl delete pod 'pod-name'`


>  Note: If components have been deployed by regular means, deployment should bring the pod and container back up.